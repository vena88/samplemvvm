# README #

A two player game!
Player 1 inputs a text. Text is converted into one animated GIFs.
Player 2 looks at image and tries to guess what the original text was.
Player 2 gets a score based on proximity between guess and the original text.

GIFs are fetched via Giphy.
https://developers.giphy.com/docs/api

Notes:
- MVVM Architecture
- I have now created a 2 player giphy game with onboarding instructions
- I used external library "Giphy" -> to get the random gif images, to get array of related terms for the search tag 
- I used external Library "SwiftGifOrigin" -> to ge the gif Image URL and show the giphy view
- I have API requests with valid API key and parse data in my model
- I have Enviroment(Debug & release)
- I have some reusable views for buttons, card view, AlertViews, RoundedImage
- Implemented to play the game and check for predictions from 100 - 0 percent 
  (If any word matches the "array of related words" form giphy endpoint then I have fixed 80%)
  (I also check for nearest word and have prediction percentages)
  (If 0% match then player 1 is the winner)
